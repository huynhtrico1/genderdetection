const fs = require('fs');
var $ = jQuery = require('jquery');
$.csv = require('jquery-csv');
const fastcsv = require('fast-csv');
var async = require('async');

var convert = function(str) {
    var result = [];
    str = str.split(" ");
    for (var i = 1; i < str.length; i++) {
        var toNumber = "";
        for(var j = 0; j < str[i].length; j++) {
          let item = str[i][j].charCodeAt();
            toNumber += item;
        }
        result.push(parseInt(toNumber));
    }
    return result;
}

function readFile2(x, callback) {
    var result = [];
    fs.readFile(x, 'UTF-8', function(err, csv) {
        $.csv.toArrays(csv, {}, function(err, data) {
          for(var i = 1, len = data.length; i < data.length; i++) {
            result.push(data[i]);
          }
          return callback(null, result);
        });
    });
}

var listData = ["./datatraining/raw_inbox_0_500.csv", "./datatraining/raw_inbox_500_500.csv", "./datatraining/raw_inbox_1000_500.csv",
                "./datatraining/raw_inbox_1500_500.csv","./datatraining/raw_inbox_2000_500.csv", "./datatraining/raw_inbox_2500_500.csv",
                "./datatraining/raw_inbox_3000_500.csv", "./datatraining/raw_inbox_3500_500.csv", "./datatraining/raw_inbox_4000_500.csv",
                "./datatraining/raw_inbox_4500_500.csv", "./datatraining/raw_inbox_5000_500.csv", "./datatraining/raw_inbox_5500_500.csv",
                "./datatraining/raw_inbox_6000_500.csv", "./datatraining/raw_inbox_6500_500.csv", "./datatraining/raw_inbox_7000_500.csv",
                "./datatraining/raw_inbox_7500_500.csv", "./datatraining/raw_inbox_8000_500.csv", "./datatraining/raw_inbox_8500_500.csv",
                "./datatraining/raw_inbox_9000_500.csv", "./datatraining/raw_inbox_9500_500.csv"];

var lenMax = (arrNames) => {
    var lenMax = arrNames[0].length;
    for(var i = 1; i < arrNames.length - 1 ; i++) {
        if(arrNames[i].length > lenMax) {
            lenMax = arrNames[i].length;
        }
    }
    return lenMax;
}
var formatTraingData = (arrNames, lenMax) => {
    //console.log("delta: ", lenMax - arrNames[0].length);
    for(var i = 0; i < arrNames.length; i++) {
        if(arrNames[i].length < lenMax) {
            var delta = lenMax - arrNames[i].length;
            for(var j = 0; j < delta; j++) {
                arrNames[i].push(0);
            }
        }
    }
    return arrNames;
}

function DatandLabel(names, n) {
    var result = names.map(item => ({...item}));
    var finalResult = [];
    for(var i = 0; i < n; i++) {
        finalResult.push(result[i]);
    }
    return finalResult;
}

var readFile = function (listData, callback) {
    var result = [];
    async.each(listData, function(data, cb) {
        readFile2(data, function(err, body) {
            result.push(body);
            cb(null);
        })
    }, function(err) {
        var finalResult = [];
        for(var i = 0; i < result.length; i++) {
            for(var j = 0; j < result[i].length; j++) {
                finalResult.push(result[i][j]);
            }
        }
        //console.log("result: ", finalResult);
        return callback(null, finalResult);
    })
}

function removeNull(arr) {
    var result = [];
    for(var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].filter(item => item !== Infinity && item);
        result.push(arr1);
    }
    return result;
}

function countNull(arr) {
    var count = 0;
    for(var i = 0; i < arr.length; i++) {
        for(var j = 0; j < arr[i].length; j++) {
            if(arr[i][j] === Infinity ) {
                count ++;
            }
        }
    }
    return  count;
}

readFile(listData, function(err, result) {
    dataText = DatandLabel(result, result.length);

    var words = result.map(item => convert(item[0]));
    //console.log("After spliting: ", words);
    var arrWords = removeNull(words);
    var MaxArrWords = lenMax(arrWords);
    var trainingSet = formatTraingData(arrWords, MaxArrWords);
    var dtandLb = DatandLabel(trainingSet, trainingSet.length);
    const ws = fs.createWriteStream("result.csv");
    fastcsv
        .write(dataText, { headers: true })
        .pipe(ws);
})
