const fs = require('fs');
var $ = jQuery = require('jquery');
$.csv = require('jquery-csv');
const fastcsv = require('fast-csv');
var async = require('async');
const { DecisionTreeClassifier } =  require('ml-cart');
const ConfusionMatrix = require('ml-confusion-matrix');
const crossValidation = require('ml-cross-validation');
const KNN = require('./ml-knn');
var { CountVectorizer }  = require('machinelearn/feature_extraction');

var convert = function(str) {
    var result = [];
    str = str.split(" ");
    for (var i = 1; i < str.length; i++) {
        var toNumber = "";
        //var toNumber = 0;
        for(var j = 0; j < str[i].length; j++) {
           let item = str[i][j].charCodeAt();
           //let item = str[i][j].charCodeAt();
           toNumber += item;
        }
        result.push(parseInt(toNumber));
        //result.push(toNumber);
    }
    return result;
}

function readFile2(x, callback) {
    var result = [];
    fs.readFile(x, 'UTF-8', function(err, csv) {
        $.csv.toArrays(csv, {}, function(err, data) {
          for(var i = 0, len = data.length; i < data.length; i++) {
            result.push(data[i]);
          }
          return callback(null, result);
        });
    });
}

var listData = ["./name_data/name_gt.csv"];


var readFile = function (listData, callback) {
    var result = [];
    async.each(listData, function(data, cb) {
        readFile2(data, function(err, body) {
            result.push(body);
            cb(null);
        })
    }, function(err) {
        var finalResult = [];
        for(var i = 0; i < result.length; i++) {
            for(var j = 0; j < result[i].length; j++) {
                finalResult.push(result[i][j]);
            }
        }
        //console.log("result: ", finalResult);
        return callback(null, finalResult);
    })
}

var rateOfResult = (predictions, result) => {
    return predictions.filter((prediction, index) => prediction === result[index]).length/predictions.length;
}

var getTwoFinalWorld = (array) => {
    var result = [];
    for(var i = 0; i < array.length; i++) {
        var item = "";
        var split = array[i].split(" ");
        item = split[split.length - 2] + " " + split[split.length - 1];
        result.push(item);
    }
    return result;
}

var formatInput = (Arr) => {
    //var ArrName = getTwoFinalWorld(Arr);
    var result = [];
    for(var i = 0; i < Arr.length; i++) {
        result.push(convert(Arr[i]));
    }
    return result;
}
var formatInputCountVector = (Arr) => {
    //var ArrName = getTwoFinalWorld(Arr);
    var result = [];
    for(var i = 0; i < Arr.length; i++) {
        result.push(Arr[i]);
    }
    return result;
}

var hashCode = function(s) {
    var h = 0, l = s.length, i = 0;
    if ( l > 0 )
      while (i < l)
        h = (h << 5) - h + s.charCodeAt(i++) | 0;
    return h;
  };
  
var formatUser = (names, result, k) => {
    var finalResult = [];
    for (var i = 0 ; i < names.length ; i++) {
        var item = {
            name : names[i],
            gender: result[i].predict,
            confidence: result[i].maxPoint/k
        }
        finalResult.push(item);
    }
    return finalResult;
}

var lenMax = (arrNames) => {
    var lenMax = arrNames[0].length;
    for(var i = 1; i < arrNames.length - 1 ; i++) {
        if(arrNames[i].length > lenMax) {
            lenMax = arrNames[i].length;
        }
    }
    return lenMax;
}

var formatTraingData = (arrNames, lenMax) => {
    //console.log("delta: ", lenMax - arrNames[0].length);
    for(var i = 0; i < arrNames.length; i++) {
        if(arrNames[i].length < lenMax) {
            var delta = lenMax - arrNames[i].length;
            for(var j = 0; j < delta; j++) {
                arrNames[i].push(0);
            }
        }
    }
    return arrNames;
}

var countNull = (names) => {
    var result = {
        count: 0,
        data: null,
        row : null,
        col: null
    }
    for(var i = 0; i < names.length; i++) {
        for(var j = 0; j < names[i].length; j++) {
            if(!names[i][j]) {
                result.count++;
                result.data = names[i][j];
                result.row = i;
                result.col = j;
            }
        }
    }
    return result;
}

function Float64ArrayToArray(arr) {
    var result = [];
    for(var key in arr) {
        var item = [];
        for (var key2 in arr[key]) {
            item.push(arr[key][key2]);
        }
        result.push(item);
    }
    return result;
}

function upperCase(arrText) {
    var result = [];
    for(var i = 0; i < arrText.length; i++) {
        result.push(xoa_dau(arrText[i].toUpperCase()));
    }
    return result;
}

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}

function DatandLabel(names, genders, n) {
    names.map((item, index) => item.push(genders[index]));
    var result = names.map(item => ({...item}));
    var finalResult = [];
    for(var i = 0; i < n; i++) {
        finalResult.push(result[i]);
    }
    return finalResult;
}

function formatResult (arr) {
    var x = arr.map(item => item.predict);
    return x;
}

function checkUser (arr) {
    var result = [];
    for(var i = 0; i < arr.length; i++) {
        if(arr[i].confidence <= 0.5)
            arr[i].gender = 2;
    }
    for(var i = 0; i < arr.length; i++) {
        if(arr[i].gender === 0)
            arr[i].gender = "girl";
        else if(arr[i].gender === 1)
            arr[i].gender = "boy";
        else 
            arr[i].gender = "undefined"
    }

    for(var i = 0; i < arr.length; i++) {
        // if(arr[i].gender !== "undefined")
            result.push(arr[i]);
    }
    return result;
}

function balanceData(data) {
    var result = [];
    var boys = data.filter(item => +item[2] === 1);
    var girls = data.filter(item => +item[2] === 0);
    for(var i = 0; i < boys.length; i++) {
        result.push(boys[i]);
        result.push(girls[i]);
    }
    return result;
}

function splitData(str) {
    var result = [];
    str = str.split(" ");
    for (var i = 1; i < str.length; i++) {
        result.push(str[i]);
    }
    return result;
}

function dictionary(data) {
    var arr = new Set();
    for(let i = 0; i < data.length; i++) {
        for(let j = 0; j < data[i].length; j++) {
            if(arr.has(data[i][j])) 
                continue;
            else {
                arr.add(data[i][j]);
            }
        }
    }
    return arr;
}

function countVectorizer(names, dics) {
    var result = [];
    for(var i = 0; i < names.length; i++) {
        var item = new Array(dics.length);
        for(let j = 0; j < item.length; j++) {
            item[i] = 0;
        }
        for(let j = 0; j < item.length; j++) {
            if(names[i].indexOf(dics[j]) >= 0)
                item[i] = 1;
        }
        result.push(item);
    }
    return result;
}

function cutArray(names, genders, len, types) {
    var result = [];
    if(types === 'names') {
        for(var i = 0; i < len ; i++) {
            result.push(names[i])
        }
    }
    else {
        for(var i = 0; i < len ; i++) {
            result.push(genders[i])
        }
    }
    return  result;
}

function checkSex(number) {
    switch(number) {
        case 1: return "boy"
        case 0: return "girl"
        default: "undefined"
    }
}

// var text1 = ["Võ Duy Cận","Phạm Thanh Tùng",'Nguyễn Duy Khương','Huỳnh Trí Cơ',
//                 'Trần Thế Duy', 'Nguyễn Doãn Thanh Chương','Nguyễn Hoàng Nam','Nguyễn Thị Kim Yến',
//                 'Trần Thị Kim Thanh','Ngô Thị Diễm Phúc','Trần Văn Phong','Trần Thị Minh Thư',
//                 'Võ Nhật Kim Hoàng','Trần Thị Ngọc Ánh','Nguyễn Thị Hoài Thương','Nguyễn Thị Hoài Phúc',
//                 'Trần Thị Kim Loan','Trần Mỹ Duyên','Ngô Phước Thịnh','Phương Bích Chi', 
//                 'Nguyễn Thị Thúy Nga','Trần Nguyễn Phương Linh',"Nguyễn Tấn Phương", "Lê Mai Văn Khánh", 
//                 "Hồ Nguyên Bảo", "Trần Thị Trâm Anh", "Nguyễn Việt Dũng", "Phạm Trung Chánh",
//                 "Nguyễn Đỗ Thị Yến Nhi", "Phan Thị Ánh Sương","Trần Đức Vân","Ngô Đức Kha",
//                 "Đinh Gia Khánh","Hoàng Đức Đạt"];
// var label = [ 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1,1,1];

function genderDetectionKNN(text, callback) {
    readFile(listData, function(err, data) {
        if(err) return callback(err);

        const names = data.map(item => convert(item[0]));
        const genders = data.map(item => +item[2]);


        var lenMaxNames = lenMax(names);
    
        var formatText = formatInput(upperCase(text));

        var lenMaxText = lenMax(formatText);
        var maximumLength;
        if(lenMaxNames > lenMaxText)
            maximumLength = lenMaxNames;
        else
            maximumLength = lenMaxText;
        var trainingSet = formatTraingData(names, maximumLength);
        var testingData = formatTraingData(formatText, maximumLength);


        const knn = new KNN(trainingSet, genders, {k: 10});

        var numberOfK = knn.toJSON().k;
        const kq = knn.predict(testingData);
    
   
        var resultUser = checkUser(formatUser(text, kq, numberOfK))
        return callback(null, resultUser);
    })
}

function genderDectectionKNNandCV(text, callback) {
     readFile(listData, function(err, data) {
        if(err) return callback(err);


        const names = data.map(item => item[0]);
        const genders = data.map(item => +item[2]);

        var training = cutArray(names, genders, 20000, 'names');
        var rstraining = cutArray(names, genders, 20000, 'genders');

        var cv = new CountVectorizer;
        var trainingSet = cv.fit_transform(training);
        
         var formatText = formatInputCountVector(upperCase(text));

        var testingData = cv.transform(formatText);

        const knn = new KNN(trainingSet, rstraining, {k: 10});
        var numberOfK = knn.toJSON().k;
        const kq = knn.predict(testingData);

        var result = kq.map((item, index) => ({name: text[index], gender : checkSex(item.predict), confidence: item.maxPoint/numberOfK}));

        return callback(null, result);
    })
}

module.exports = {
    genderDetectionKNN,
    genderDectectionKNNandCV
}



